#! /bin/sh

notify() {
   MESSAGE=$(deno run --allow-net ~/Code/git/m8bot/m8bot.js)
   if [ "$MESSAGE" != 'no updates' ]; then
      terminal-notifier -message "$MESSAGE" -title "M8 IN STOCK"
   else
      echo "no updates"
   fi
}

while true; do notify & sleep 60; done
