import { DOMParser } from 'https://deno.land/x/deno_dom/deno-dom-wasm.ts'

const html = await fetch("https://dirtywave.com/products/m8-tracker")
  .then(res => res.text())
const doc = new DOMParser()
  .parseFromString(html, 'text/html')
const [description, _] = doc.getElementsByTagName('meta')
  .filter(x => x.attributes.name?.includes('description'))
  .filter(x => !x.attributes.name.includes(':'))
  .map(x => x.attributes.content)

const soldOut = description.includes('M8 is currently sold out.')

soldOut ?
  console.log('no updates') :
  console.log('✨restocked✨ get that shiiit https://dirtywave.com/products/m8-tracker')
